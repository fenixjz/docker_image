# Docker image Win 10 x64

  - Ubuntu 16.04
  - PHP 7.2
  - MySQL 5.7.23
  - MSSQL mssql-server-linux
  - PHPMyAdmin
  - Composer
  
### Useful links

| Name | Link |
| ------ | ------ |
| Docker Desktop for Windows | [[link](https://hub.docker.com/editions/community/docker-ce-desktop-windows)] |
| DockStation | [[link](https://dockstation.io/)] |
| HTTPD | [[link](https://hub.docker.com/_/httpd)] |
| PHP | [[link](https://hub.docker.com/_/php/)] |
| MySQL | [[link](https://hub.docker.com/_/mysql)] |
| MSSQL | [[link](https://hub.docker.com/r/microsoft/mssql-server-linux/)] |
| PHPMyAdmin | [[link](https://hub.docker.com/r/phpmyadmin/phpmyadmin)] |
| Composer | [[link](https://hub.docker.com/_/composer)] |

### Please change PORTS, PATHS and CERTIFICATES for personal use!